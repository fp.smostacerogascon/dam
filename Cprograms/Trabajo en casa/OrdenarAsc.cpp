#include <iostream>
using namespace std;

void ordenarTresNumeros(int &a, int &b, int &c) {
    if (a > b) swap(a, b);
    if (a > c) swap(a, c);
    if (b > c) swap(b, c);
}

int main() {
    int num1, num2, num3;
    cout << "Ingresa tres numeros: ";
    cin >> num1 >> num2 >> num3;

    ordenarTresNumeros(num1, num2, num3);

    cout << "Los numeros ordenados son: " << num1 << ", " << num2 << ", " << num3 << endl;

    return 0;
}
