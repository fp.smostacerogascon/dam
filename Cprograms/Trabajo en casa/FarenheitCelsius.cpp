#include <iostream>
using namespace std;

double celsiusAFahrenheit(double celsius) {
    return (celsius * 9.0 / 5.0) + 32.0;
}

int main() {
    double celsius;
    cout << "Ingresa la temperatura en grados Celsius: ";
    cin >> celsius;

    double fahrenheit = celsiusAFahrenheit(celsius);
    cout << "La temperatura en grados Fahrenheit es: " << fahrenheit << endl;

    return 0;
}
