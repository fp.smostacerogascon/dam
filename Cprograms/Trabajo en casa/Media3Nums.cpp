#include <iostream>
using namespace std;

double calcularPromedio(double a, double b, double c) {
    return (a + b + c) / 3.0;
}

int main() {
    double num1, num2, num3;
    cout << "Ingresa tres numeros para calcular su promedio: ";
    cin >> num1 >> num2 >> num3;

    cout << "El promedio es: " << calcularPromedio(num1, num2, num3) << endl;

    return 0;
}
