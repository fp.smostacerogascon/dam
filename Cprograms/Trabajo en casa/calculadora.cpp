#include <iostream>

using namespace std;

void mostrarMenu() {
    cout << "Calculadora\n";
    cout << "1. Suma\n";
    cout << "2. Resta\n";
    cout << "3. Multiplicacion\n";
    cout << "4. Division\n";
    cout << "5. Salir\n";
    cout << "Selecciona una opcion: ";
}

int main() {
    int opcion;
    double num1, num2;
    
    while (true) {
        mostrarMenu();
        cin >> opcion;

        if (opcion == 5) {
            cout << "Saliendo de la calculadora..." << endl;
            break;
        }

        if (opcion < 1 || opcion > 5) {
            cout << "Opcion invalida. Por favor, intenta de nuevo.\n";
            continue;
        }

        cout << "Ingresa el primer numero: ";
        cin >> num1;
        cout << "Ingresa el segundo numero: ";
        cin >> num2;

        switch (opcion) {
            case 1:
                cout << "Resultado: " << num1 + num2 << endl;
                break;
            case 2:
                cout << "Resultado: " << num1 - num2 << endl;
                break;
            case 3:
                cout << "Resultado: " << num1 * num2 << endl;
                break;
            case 4:
                if (num2 != 0)
                    cout << "Resultado: " << num1 / num2 << endl;
                else
                    cout << "Error: Division por cero no permitida.\n";
                break;
            default:
                cout << "Opcion invalida. Por favor, intenta de nuevo.\n";
                break;
        }
    }

    return 0;
}
