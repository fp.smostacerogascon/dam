#include <iostream>
using namespace std;

int sumaHastaN(int n) {
    return n * (n + 1) / 2; // F�rmula para la suma de los primeros N n�meros naturales
}

int main() {
    int numero;
    cout << "Ingresa un numero para calcular la suma de los primeros N numeros naturales: ";
    cin >> numero;

    if (numero < 1) {
        cout << "Por favor, ingresa un numero mayor o igual a 1." << endl;
    } else {
        cout << "La suma de los primeros " << numero << " numeros naturales es " << sumaHastaN(numero) << endl;
    }

    return 0;
}
