#include <iostream>
#include <string>
using namespace std;

int contarVocales(const string& texto) {
    int contador = 0;
    for (char c : texto) {
        if (tolower(c) == 'a' || tolower(c) == 'e' || tolower(c) == 'i' || tolower(c) == 'o' || tolower(c) == 'u') {
            contador++;
        }
    }
    return contador;
}

int main() {
    string texto;
    cout << "Ingresa una cadena de texto: ";
    getline(cin, texto);

    cout << "La cantidad de vocales en la cadena es: " << contarVocales(texto) << endl;

    return 0;
}
