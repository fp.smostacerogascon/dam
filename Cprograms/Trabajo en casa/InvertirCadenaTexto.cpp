#include <iostream>
#include <string>
using namespace std;

string invertirCadena(const string& cadena) {
    string invertida = cadena;
    int n = cadena.length();
    for (int i = 0; i < n / 2; ++i) {
        swap(invertida[i], invertida[n - i - 1]);
    }
    return invertida;
}

int main() {
    string cadena;
    cout << "Ingresa una cadena de texto: ";
    cin >> cadena;

    cout << "La cadena invertida es: " << invertirCadena(cadena) << endl;

    return 0;
}
