#include <iostream>
using namespace std;

// Clase base
class Animal {
public:
    virtual void hacerSonido() const {
        cout << "El animal hace un sonido" << endl;
    }
    virtual ~Animal() {} // Destructor virtual para asegurar la destrucci�n adecuada
};

// Clase derivada Perro
class Perro : public Animal {
public:
    void hacerSonido() const override {
        cout << "El perro ladra: Guau guau" << endl;
    }
};

// Clase derivada Gato
class Gato : public Animal {
public:
    void hacerSonido() const override {
        cout << "El gato ma�lla: Miau miau" << endl;
    }
};

// Funci�n principal
int main() {
    Animal* miAnimal;
    Perro miPerro;
    Gato miGato;

    // Apuntar al objeto Perro
    miAnimal = &miPerro;
    miAnimal->hacerSonido();  // Llama a Perro::hacerSonido

    // Apuntar al objeto Gato
    miAnimal = &miGato;
    miAnimal->hacerSonido();  // Llama a Gato::hacerSonido

    return 0;
}
