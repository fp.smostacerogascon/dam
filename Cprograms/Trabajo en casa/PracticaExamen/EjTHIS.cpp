#include <iostream>
using namespace std;

class Ejemplo {
public:
    int dato;

    Ejemplo(int dato) {
        this->dato = dato;
    }

    void mostrarDato() {
        cout << "El dato es: " << this->dato << endl;
    }
};

int main() {
    Ejemplo objeto(42);
    objeto.mostrarDato();

    return 0;
}
