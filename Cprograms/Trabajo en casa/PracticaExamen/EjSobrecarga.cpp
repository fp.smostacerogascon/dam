#include <iostream>
#include <cmath>
using namespace std;

const double PI = 3.14159265358979323846;

// Funci�n para calcular el �rea de un cuadrado
double calcularArea(double lado) {
    return lado * lado;
}

// Funci�n para calcular el �rea de un rect�ngulo
double calcularArea(double base, double altura) {
    return base * altura;
}

// Funci�n para calcular el �rea de un c�rculo
double calcularArea(double radio) {
    return PI * pow(radio, 2);
}

int main() {
    double lado, base, altura, radio;
    
    // �rea de un cuadrado
    lado = 5.0;
    cout << "Area de un cuadrado con lado " << lado << ": " << calcularArea(lado) << endl;

    // �rea de un rect�ngulo
    base = 6.0;
    altura = 4.0;
    cout << "Area de un rectangulo con base " << base << " y altura " << altura << ": " << calcularArea(base, altura) << endl;

    // �rea de un c�rculo
    radio = 3.0;
    cout << "Area de un circulo con radio " << radio << ": " << calcularArea(radio) << endl;

    return 0;
}
