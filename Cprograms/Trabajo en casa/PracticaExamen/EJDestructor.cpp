#include <iostream>
using namespace std;

class Persona {
private:
    string nombre;

public:
    Persona(const string& n) : nombre(n) {
        cout << "Constructor llamado para " << nombre << endl;
    }

    ~Persona() {
        cout << "Destructor llamado para " << nombre << endl;
    }

    void mostrarNombre() const {
        cout << "Nombre: " << nombre << endl;
    }
};

int main() {
    Persona p1("Alice");
    Persona p2("Bob");

    p1.mostrarNombre();
    p2.mostrarNombre();

    return 0;
}
