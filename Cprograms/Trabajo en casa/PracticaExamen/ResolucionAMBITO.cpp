#include <iostream>
using namespace std;

int variableGlobal = 10;

void funcion1() {
    int variableLocal1 = 20;
    printf("En funcion1, variableGlobal = %d\n", variableGlobal);
    printf("En funcion1, variableLocal1 = %d\n", variableLocal1);
}

void funcion2() {
    int variableLocal2 = 30;
    printf("En funcion2, variableGlobal = %d\n", variableGlobal);
    printf("En funcion2, variableLocal2 = %d\n", variableLocal2);
}

class Clase {
public:
    int miMiembro;
    
    void miMetodo() {
        printf("En miMetodo, miMiembro = %d\n", miMiembro);
        printf("En miMetodo, variableGlobal = %d\n", variableGlobal);
    }
};

int main() {
    printf("En main, variableGlobal = %d\n", variableGlobal);

    funcion1();
    funcion2();

    Clase objeto;
    objeto.miMiembro = 42;
    objeto.miMetodo();

    return 0;
}
