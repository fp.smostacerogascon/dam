#include <iostream>
using namespace std;

const double PI = 3.14159265358979323846;

double calcularAreaCirculo(double radio) {
    return PI * radio * radio;
}

int main() {
    double radio;
    cout << "Ingresa el radio del circulo: ";
    cin >> radio;

    cout << "El area del circulo es: " << calcularAreaCirculo(radio) << endl;

    return 0;
}
