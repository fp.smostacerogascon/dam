#include <stdio.h>

// Funci�n para imprimir el tablero
void imprimirTablero(char tablero[3][3]) {
    printf(" %c | %c | %c\n", tablero[0][0], tablero[0][1], tablero[0][2]);
    printf("---|---|---\n");
    printf(" %c | %c | %c\n", tablero[1][0], tablero[1][1], tablero[1][2]);
    printf("---|---|---\n");
    printf(" %c | %c | %c\n", tablero[2][0], tablero[2][1], tablero[2][2]);
}

// Funci�n para verificar si hay un ganador
char verificarGanador(char tablero[3][3]) {
    // Revisar filas
    for (int i = 0; i < 3; i++) {
        if (tablero[i][0] == tablero[i][1] && tablero[i][1] == tablero[i][2])
            return tablero[i][0];
    }
    // Revisar columnas
    for (int i = 0; i < 3; i++) {
        if (tablero[0][i] == tablero[1][i] && tablero[1][i] == tablero[2][i])
            return tablero[0][i];
    }
    // Revisar diagonales
    if (tablero[0][0] == tablero[1][1] && tablero[1][1] == tablero[2][2])
        return tablero[0][0];
    if (tablero[0][2] == tablero[1][1] && tablero[1][1] == tablero[2][0])
        return tablero[0][2];
    
    return ' ';
}

// Funci�n principal
int main() {
    char tablero[3][3] = { {'1', '2', '3'}, {'4', '5', '6'}, {'7', '8', '9'} };
    char turno = 'X';
    int movimiento, fila, columna;
    char ganador = ' ';

    for (int i = 0; i < 9 && ganador == ' '; i++) {
        imprimirTablero(tablero);
        printf("Turno del jugador %c. Ingresa el numero de la casilla: ", turno);
        scanf("%d", &movimiento);

        // Convertir el movimiento a fila y columna
        fila = (movimiento - 1) / 3;
        columna = (movimiento - 1) % 3;

        // Validar el movimiento
        if (tablero[fila][columna] != 'X' && tablero[fila][columna] != 'O') {
            tablero[fila][columna] = turno;
            turno = (turno == 'X') ? 'O' : 'X';
        } else {
            printf("Movimiento invalido! Intenta de nuevo.\n");
            i--;
        }

        ganador = verificarGanador(tablero);
    }

    imprimirTablero(tablero);
    if (ganador != ' ')
        printf("�El jugador %c gana!\n", ganador);
    else
        printf("�Es un empate!\n");

    return 0;
}
