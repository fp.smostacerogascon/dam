#include <iostream>
using namespace std;

bool esBisiesto(int year) {
    return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);
}

int main() {
    int year;
    cout << "Ingresa un a�o: ";
    cin >> year;

    if (esBisiesto(year)) {
        cout << year << " es un a�o bisiesto." << endl;
    } else {
        cout << year << " no es un a�o bisiesto." << endl;
    }

    return 0;
}
