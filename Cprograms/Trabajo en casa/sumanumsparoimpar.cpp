#include <iostream>

using namespace std;

int main() {
    int num1, num2, suma;

    // Solicitar al usuario que ingrese los dos n�meros
    cout << "Ingresa el primer numero: ";
    cin >> num1;
    cout << "Ingresa el segundo numero: ";
    cin >> num2;

    // Calcular la suma de los dos n�meros
    suma = num1 + num2;

    // Mostrar la suma
    cout << "La suma de " << num1 << " y " << num2 << " es " << suma << "." << endl;

    // Verificar si la suma es par o impar
    if (suma % 2 == 0) {
        cout << "La suma " << suma << " es par." << endl;
    } else {
        cout << "La suma " << suma << " es impar." << endl;
    }

    return 0;
}
