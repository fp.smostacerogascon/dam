#include <iostream>
using namespace std;

double celsiusAFahrenheit(double celsius) {
    return (celsius * 9.0 / 5.0) + 32.0;
}

double celsiusAKelvin(double celsius) {
    return celsius + 273.15;
}

double celsiusARankine(double celsius) {
    return (celsius + 273.15) * 9.0 / 5.0;
}

double celsiusAReaumur(double celsius) {
    return celsius * 4.0 / 5.0;
}

int main() {
    double celsius;
    cout << "Ingresa la temperatura en grados Celsius: ";
    cin >> celsius;

    cout << "Fahrenheit: " << celsiusAFahrenheit(celsius) << endl;
    cout << "Kelvin: " << celsiusAKelvin(celsius) << endl;
    cout << "Rankine: " << celsiusARankine(celsius) << endl;
    cout << "R�aumur: " << celsiusAReaumur(celsius) << endl;

    return 0;
}
