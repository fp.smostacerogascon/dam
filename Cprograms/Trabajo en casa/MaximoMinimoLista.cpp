#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

void encontrarMaxMin(const vector<int>& numeros, int& maximo, int& minimo) {
    maximo = *max_element(numeros.begin(), numeros.end());
    minimo = *min_element(numeros.begin(), numeros.end());
}

int main() {
    int tamano;
    cout << "Ingresa la cantidad de numeros: ";
    cin >> tamano;

    vector<int> numeros(tamano);
    cout << "Ingresa los numeros: ";
    for (int i = 0; i < tamano; ++i) {
        cin >> numeros[i];
    }

    int maximo, minimo;
    encontrarMaxMin(numeros, maximo, minimo);
    cout << "El numero maximo es: " << maximo << endl;
    cout << "El numero minimo es: " << minimo << endl;

    return 0;
}
