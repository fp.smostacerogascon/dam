#include <iostream>
using namespace std;

int factorial(int n) {
    int resultado = 1;
    for (int i = 1; i <= n; ++i) {
        resultado *= i;
    }
    return resultado;
}

int main() {
    int numero;
    cout << "Ingresa un numero para calcular su factorial: ";
    cin >> numero;

    if (numero < 0) {
        cout << "El factorial no esta definido para numeros negativos." << endl;
    } else {
        cout << "El factorial de " << numero << " es " << factorial(numero) << endl;
    }

    return 0;
}
