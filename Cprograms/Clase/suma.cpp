#include<stdio.h>
#include<stdlib.h>

void usage (FILE *where, const char *error_mssg){

    fprintf(where, "ERROR: %s!!!\n", error_mssg);
    if(where ==stderr)
        exit (1);

}


int main(int argc, char *argv[] ) {
    int res=0; 

    if (argc < 2)
        usage(stderr, "Se necesita al menos un argumento");

    for (int i=1; i<argc; i++)
         res += atoi(argv[i]);

    printf("%i\n", res);

    return EXIT_SUCCESS;
    
  }
