#include <stdio.h>
#include <stdlib.h>
#define NUMLETRAS ('z' - 'a' + 1) //a - a seria 0, asiq se suma 1 para que A=1 B=2..

int main(int argc, char *argv[]){
   char abc[NUMLETRAS];
   //recorrer celda a celda
   //meter en la celda 0 meter la letra 0 por atras
   //meter e la celda 1 la letra 1 empezando por atras
   //meter en la celda i la letra i empezando por atras

   //calcular
   for(char i='z'; i>='a'; i--)
       abc['z' - i]=i;
  //imprimir
   for(char i='a'; i<='z'; i++)
       printf("%c ", abc[i - 'a']);
   printf("\n");



    return EXIT_SUCCESS;
}
